* This code is made to convert/process fitbit steps data
* It loads the .json file exported from fitbit.com

# Following adjustments have to be made:
* -Adjust path for json data 
* -Enter start date and end date of time period you want to analyze 
* -Adjust min. step count (-> minsteps) which is needed that day counts into mean daily step count
* -Adjust sampling rate (SR): depending on what time intervals you want the data to be sampled

# Outputs
* 1) SUM OF STEPS PER DAY -> PLOT/DATA FOR SUM OF STEPS PER DAY 
*        -> daily_steps     
* 2) MEAN DAILY STEPS WITHOUT DAYS SMALLER THAN A CERTAIN VALUE(MINSTEPS)
*       -> mean_daily_steps
* 3) GRAPHS FOR STEPS OF EVERY SINGLE DAY
* 4) GRAPH/DATA FOR AVERAGE DAY: RESAMPLED DATA FOR A CERTAIN TIME PERIOND (SR) CALCULATED MEAN AND STANDARD DEVIATION FOR AN AVERAGE DAY
*       -> data: first row: mean, second row: SD positive, third row: SD negative

