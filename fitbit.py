#INPUT: JSON FILE OF FITBIT DATA
#OUTPUT: 
# 1) SUM OF STEPS PER DAY -> PLOT/DATA FOR SUM OF STEPS PER DAY 
#       -> daily_steps     
# 2) MEAN DAILY STEPS WITHOUT DAYS SMALLER THAN A CERTAIN VALUE(MINSTEPS)
#       -> mean_daily_steps
# 3) GRAPHS FOR STEPS OF EVERY SINGLE DAY
# 4) GRAPH/DATA FOR AVERAGE DAY: RESAMPLED DATA FOR A CERTAIN TIME PERIOND (SR)
#    CALCULATED MEAN AND STANDARD DEVIATION FOR AN AVERAGE DAY
#       -> data: first row: mean, second row: SD positive, third row: SD negative

import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np
import json
# =============================================================================
# LOAD DATA
with open("//hest.nas.ethz.ch/green_groups_sms_public/Projects/Myosuit/MyosuitFull/05_Studies_and_Experiments/TRAIN_TO_WALK/TWP_01/03_EXPERIMENTAL_DATA/FITBIT_DATA/steps-2019-09-14.json", 'r') as f:
    fitbit = json.load(f)
fitbit2 = json.dumps(fitbit)
df = pd.read_json(fitbit2)

#split date and time and change data type
df['Date'],df['Time']= df['dateTime'].apply(lambda x:x.date()), df['dateTime'].apply(lambda x:x.time())
df["Date"] = pd.to_datetime(df["Date"])

print(df.dtypes)

#fill in start and end date
#data before and after is being deleted
date_start = "9.14.19" #MM.DD.YY (month without zero: 09 = 9)
date_start = pd.to_datetime(date_start)
df2_1 = df.drop(df[df.Date<date_start].index)

date_end = "10.10.19" #MM.DD.YY (month without zero: 09 = 9)
date_end = pd.to_datetime(date_end)
df2 = df2_1.drop(df2_1[df2_1.Date>date_end].index)

#fill in minimal step count one day has to have 
#that it will be counted for mean daily steps calculation
minsteps = 150 

#fill in sampling rate for average day calculation
#1hour = H
#1min = T -> 10min = 10T
#has to be string
SR = "H"

#Should daily graphs be plotted (all the steps of every day)? 
#False = no, True = yes
processdailygraphs = False

# =============================================================================

# =============================================================================
# STEPS PER DAY / MEAN DAILY STEP COUNT

daily_steps = df2.groupby("Date")["value"].sum() #sum of steps per day
daily_steps = daily_steps.asfreq("D") #add missing dates in time period to series

#plot daily steps
daily_steps.plot.bar()
plt.title("Daily Steps")
plt.xlabel("Date")
plt.ylabel("Steps")
plt.show()

#calculate mean daily step count with all days over 150 steps
mean_daily_steps = daily_steps[daily_steps > minsteps].mean() #mean daily steps over whole time period excluding dates with less than 150 steps
print("Mean daily step count is ",mean_daily_steps)
# =============================================================================

# =============================================================================
# GRAPHS PER DAY

if processdailygraphs == True:
    for title, group in df2.groupby("Date"):
        group.plot.bar(x="Time", y="value", title=title, figsize=(25,7))
        plt.xlabel("Time")
        plt.ylabel("Steps")
        plt.show()
else:
    print("Daily graphs not printed.")        
# =============================================================================

# =============================================================================
# AVERAGE DAY: calculation of mean and SD for every hour of day

resampled = df2.resample(SR, on = "dateTime").sum()
resampled = resampled.asfreq(SR) 
resampled['DateTime'] = resampled.index
resampled['Date'],resampled['Time']= resampled['DateTime'].apply(lambda x:x.date()), resampled['DateTime'].apply(lambda x:x.time())

#only use values from 07:00 to 22:00 
time_start = "7:00:00"
dt1 = datetime.strptime(time_start,"%H:%M:%S").time()
time_end = "22:00:00"
dt2 = datetime.strptime(time_end,"%H:%M:%S").time()

resampled2 = resampled.drop(resampled[resampled.Time<dt1].index)
resampled2 = resampled2.drop(resampled2[resampled2.Time>dt2].index)

#calculate mean and SD of an average day
resampled_mean = resampled2.groupby("Time")["value"].mean()
resampled_SD = resampled2.groupby("Time")["value"].std()


#if SD > mean -> make negative SD = mean
resampled_SDpos = resampled_SD
resampled_SDneg0 = pd.Series(np.linspace(0, 0, 16))
resampled_SDneg = resampled_SDneg0.reindex(index=resampled_SDpos.index)

for i in range(len(resampled_SDneg)):
   if resampled_SD[i] > resampled_mean[i]:
        resampled_SDneg[i] = resampled_mean[i]
   else:
        resampled_SDneg[i] = resampled_SD[i]


#plot 
x_pos = np.arange(len(resampled_mean))

fig, ax = plt.subplots()
ax.bar(x_pos, resampled_mean, yerr=[resampled_SDneg, resampled_SDpos], align='center', alpha=0.5, ecolor='black', capsize=10)
ax.set_ylabel('Steps')
ax.set_xticks(x_pos)
ax.set_title('AVERAGE DAY')
ax.yaxis.grid(True)
resampled_mean.plot.bar()

data = pd.concat([resampled_mean, resampled_SDpos, resampled_SDneg], axis=1).reset_index()

 